package com.example.rafalsuski.email;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Handler;


import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class MainActivity extends MenuPersonSave implements View.OnClickListener, DataClock, EmailPassword, EvidenceUser {

    String email;
    String password;
    String nameUser;

    TextView textprzebiegu;
    EditText mesage;
    EditText comments;
    EditText route;
    // EditText defects;

    Button sendStart;
    Button sendStop;
    Session session;
    // Switch switchDeffect;

    TextView connect;
    TextView courierS;
    TextView transit;
    TextView vwgolf;
    TextView courierB;

    public String stringConnect = "CONNECT LU 234FK";
    public String stringCourierS = "COURIER LU 477EU";
    public String stringTransit = "TRANSIT LU 486FT";
    public String stringVwgolf = "VW GOLF LU 0967L";
    public String stringCourierB = "COURIER LU 131FU";

    public ArrayList <String> selectCar ;

    ImageButton imageButtonConnect ;
    ImageButton imageButtonCourierS ;
    ImageButton imageButtonTransit ;
    ImageButton imageButtonGolf ;
    ImageButton imageButtonCourierB;

    public DrawerLayout mDrawerLayout;
    public ActionBarDrawerToggle mToogle;

    private long backPressedTime;



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout_user);

        //ProgressDialog dialog = new ProgressDialog(MainActivity.this); // this = YourActivity
        //dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //dialog.setMessage("Loading. Please wait...");
        //dialog.setIndeterminate(true);
        //dialog.setCanceledOnTouchOutside(false);
        //dialog.show();

           // MyTask myTask = new MyTask(this);
           // myTask.execute();


        textprzebiegu = findViewById(R.id.textprzebiegu);
        mesage = findViewById(R.id.mesage);
        comments = findViewById(R.id.comments);
        route = findViewById(R.id.route);
       // switchDeffect = findViewById(R.id.switchDeffect);
       // defects = findViewById(R.id.defects);
        sendStart = findViewById(R.id.send);
        sendStop = findViewById(R.id.sendStop);


        email = valueEmail;
        password = valuePassword;


        //final String nameUser = valueNameUser;
        //final String addressEmailUser = valueEmailUser;
        final String addressEmailAdditional = valueEmailAdditional;
        //final String addressEmailJob = valueEmailJob;


        imageButtonConnect = findViewById(R.id.imageButtonConnect);
        imageButtonCourierS = findViewById(R.id.imageButtonCourierS);
        imageButtonTransit = findViewById(R.id.imageButtonTransit);
        imageButtonGolf = findViewById(R.id.imageButtonGolf);
        imageButtonCourierB = findViewById(R.id.imageButtonCourierB);

        imageButtonConnect.setOnClickListener(this);
        imageButtonCourierS.setOnClickListener(this);
        imageButtonTransit.setOnClickListener(this);
        imageButtonGolf.setOnClickListener(this);
        imageButtonCourierB.setOnClickListener(this);

        stringConnect = "CONNECT LU 234FK";
        stringCourierS = "COURIER LU 477EU";
        stringTransit = "TRANSIT LU 486FT";
        stringVwgolf = "VW GOLF LU 0967L";
        stringCourierB = "COURIER LU 131FU";


        mesage.setEnabled(false);
        comments.setEnabled(false);
        route.setEnabled(false);
        // switchDeffect.setEnabled(false);
        // defects.setEnabled(false);
        sendStart.setEnabled(false);
        sendStop.setEnabled(false);

        imageButtonConnect.setEnabled(true);
        imageButtonCourierS.setEnabled(true);
        imageButtonTransit.setEnabled(true);
        imageButtonGolf.setEnabled(true);
        imageButtonCourierB.setEnabled(true);

        selectCar = new ArrayList<>();

        // Class don't induce keyboed start aplication.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);



        sendStart.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                Properties properties = new Properties();
                properties.put("mail.smtp.host","smtp.gmail.com");
                properties.put("mail.smtp.socketFactory.port","465");
                properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
                properties.put("mail.smtp.auth","true");
                properties.put("mail.smtp.starttls.enable","true");


                try {
                    session = Session.getDefaultInstance(properties, new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(email, password);
                        }
                    });

                    if (session!=null) {
                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(email));
                        message.setSubject(selectCar + "_" + "Pobranie pojazdu" + "_" + mesage.getText().toString() + "_" + currentDate + "_" + timeClock
                                + "_" + edit_Login_firstname.getText().toString().trim() + " "+ edit_Login_lastname.getText().toString().trim() +"_" + comments.getText().toString() + "_" + route.getText().toString());

                        // Add adress email user, additional, job

                        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse
                                (edit_Email_job.getText().toString().trim()));

                        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse
                                (addressEmailAdditional));

                        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse
                                (edit_Email_add.getText().toString().trim()));

                        message.setContent(mesage.getText().toString(), "text/html; charset=utf-8");

                        Transport transport = session.getTransport("smtp");
                        transport.connect("smtp.gmail.com",465, email, password);
                        transport.sendMessage(message,message.getAllRecipients());
                        transport.close();

                    }

                    finish();

                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getBaseContext(), "No Connection", Toast.LENGTH_SHORT).show();

                }

            }
        });


        sendStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
                Properties properties = new Properties();
                properties.put("mail.smtp.host","smtp.gmail.com");
                properties.put("mail.smtp.socketFactory.port","465");
                properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
                properties.put("mail.smtp.auth","true");
                properties.put("mail.smtp.starttls.enable","true");


                try {
                    session = Session.getDefaultInstance(properties, new Authenticator() {
                        @Override
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(email, password);
                        }
                    });

                    if (session!=null) {
                        Message message = new MimeMessage(session);
                        message.setFrom(new InternetAddress(email));
                        message.setSubject(selectCar + "_" + "Zwrot pojazdu" + "_" + mesage.getText().toString() + "_" + currentDate + "_" + timeClock
                                + "_" + nameUser + "_" + comments.getText().toString() + "_" + route.getText().toString());

                        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse
                                (edit_Email_job.getText().toString()));

                        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse
                                (addressEmailAdditional));

                        message.addRecipients(Message.RecipientType.CC, InternetAddress.parse
                                (edit_Email_add.getText().toString()));

                        message.setContent(mesage.getText().toString(), "text/html; charset=utf-8");

                        Transport transport = session.getTransport("smtp");
                        transport.connect("smtp.gmail.com",465, email, password);
                        transport.sendMessage(message,message.getAllRecipients());
                        transport.close();

                    }

                    finish();
                } catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getBaseContext(), "No Connection", Toast.LENGTH_SHORT).show();

                }
            }

        });

        // add menu item

        mDrawerLayout = findViewById(R.id.drawer);
        mToogle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mToogle);
        mToogle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    // This is implementation close press 2 click back button

    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            return;
        }

        else {
            Toast.makeText(getBaseContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

/**
    public class MyTask extends AsyncTask<String, String, String> {
        Context context;
        ProgressDialog progressDialog;

        public MyTask(Context context) {
            this.context = context;
            progressDialog = new ProgressDialog(context);
        }

        @Override
        protected void onPreExecute() {
            //progressDialog = new ProgressDialog(context);
           // progressDialog.show();
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this); // this = YourActivity
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage("Loading. Please wait...");
            progressDialog.setIndeterminate(true);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();

        }

       // @Override
        //protected void onProgressUpdate(String... values) {
           // super.onProgressUpdate(values);
        //}

        @Override
        protected String doInBackground(String... params) {
            //Do your loading here
            //SystemClock.sleep(2000);
            return (null);
            //return "finish";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressDialog.dismiss();


            //Start other Activity or do whatever you want
        }
    }
**/

    // add menu item

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mToogle.onOptionsItemSelected(item)){
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


    @SuppressLint("WrongConstant")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override

    public void onClick (View view){

        connect = findViewById(R.id.connect);
        courierS = findViewById(R.id.courierS);
        transit = findViewById(R.id.transit);
        vwgolf = findViewById(R.id.vwgolf);
        courierB = findViewById(R.id.courierB);

        final ImageButton imageButtonConnect = findViewById(R.id.imageButtonConnect);
        ImageButton imageButtonCourierS = findViewById(R.id.imageButtonCourierS);
        ImageButton imageButtonTransit = findViewById(R.id.imageButtonTransit);
        ImageButton imageButtonGolf = findViewById(R.id.imageButtonGolf);
        ImageButton imageButtonCourierB = findViewById(R.id.imageButtonCourierB);

        imageButtonConnect.setOnClickListener(this);
        imageButtonCourierS.setOnClickListener(this);
        imageButtonTransit.setOnClickListener(this);
        imageButtonGolf.setOnClickListener(this);
        imageButtonCourierB.setOnClickListener(this);

        if (mesage.isSaveEnabled()){
            sendStart.setEnabled(true);
            sendStop.setEnabled(true);

        }
        else {
           sendStart.setEnabled(false);
           sendStop.setEnabled(false);
       }


        switch (view.getId()){

            case R.id.imageButtonConnect:

                imageButtonConnect.setEnabled(true);
                imageButtonConnect.setOnClickListener(null);
                mesage.setEnabled(true);
                comments.setEnabled(true);
                route.setEnabled(true);
                // switchDeffect.setEnabled(true);
                selectCar.add(stringConnect);

                imageButtonCourierS.setVisibility(View.INVISIBLE);
                imageButtonTransit.setVisibility(View.INVISIBLE);
                imageButtonGolf.setVisibility(View.INVISIBLE);
                imageButtonCourierB.setVisibility(View.INVISIBLE);

                courierS.setVisibility(View.INVISIBLE);
                transit.setVisibility(View.INVISIBLE);
                vwgolf.setVisibility(View.INVISIBLE);
                courierB.setVisibility(View.INVISIBLE);



               final Toast toastConnect = Toast.makeText(this, "Wybrałeś" + " " + stringConnect , Toast.LENGTH_SHORT);
                toastConnect.show();

                new CountDownTimer(500, 1000)
                {
                    public void onTick(long millisUntilFinished) {
                        toastConnect.show();
                    }
                    public void onFinish() {
                        toastConnect.cancel();
                    }
                }.start();

                break;


            case R.id.imageButtonCourierS:

                imageButtonCourierS.setEnabled(true);
                imageButtonCourierS.setOnClickListener(null);
                mesage.setEnabled(true);
                comments.setEnabled(true);
                route.setEnabled(true);
                // switchDeffect.setEnabled(true);
                selectCar.add(stringCourierS);

                imageButtonConnect.setVisibility(View.INVISIBLE);
                imageButtonTransit.setVisibility(View.INVISIBLE);
                imageButtonGolf.setVisibility(View.INVISIBLE);
                imageButtonCourierB.setVisibility(View.INVISIBLE);

                connect.setVisibility(View.INVISIBLE);
                transit.setVisibility(View.INVISIBLE);
                vwgolf.setVisibility(View.INVISIBLE);
                courierB.setVisibility(View.INVISIBLE);

                final Toast toastCurierS = Toast.makeText(this, "Wybrałeś" + " " + stringCourierS , Toast.LENGTH_SHORT);
                toastCurierS.show();

                new CountDownTimer(500, 1000)
                {
                    public void onTick(long millisUntilFinished) {
                        toastCurierS.show();
                    }
                    public void onFinish() {
                        toastCurierS.cancel();
                    }
                }.start();

               break;


            case R.id.imageButtonTransit:

                imageButtonTransit.setEnabled(true);
                imageButtonTransit.setOnClickListener(null);
                mesage.setEnabled(true);
                comments.setEnabled(true);
                route.setEnabled(true);
                // switchDeffect.setEnabled(true);
                selectCar.add(stringTransit);

                imageButtonConnect.setVisibility(View.INVISIBLE);
                imageButtonCourierS.setVisibility(View.INVISIBLE);
                imageButtonGolf.setVisibility(View.INVISIBLE);
                imageButtonCourierB.setVisibility(View.INVISIBLE);

                connect.setVisibility(View.INVISIBLE);
                courierS.setVisibility(View.INVISIBLE);
                vwgolf.setVisibility(View.INVISIBLE);
                courierB.setVisibility(View.INVISIBLE);

                final Toast toastTransit = Toast.makeText(this, "Wybrałeś" + " " + stringTransit , Toast.LENGTH_SHORT);
                toastTransit.show();

                new CountDownTimer(500, 1000)
                {
                    public void onTick(long millisUntilFinished) {
                        toastTransit.show();
                    }
                    public void onFinish() {
                        toastTransit.cancel();
                    }
                }.start();

                 break;


            case R.id.imageButtonGolf:

                imageButtonGolf.setEnabled(true);
                imageButtonGolf.setOnClickListener(null);
                mesage.setEnabled(true);
                comments.setEnabled(true);
                route.setEnabled(true);
                // switchDeffect.setEnabled(true);
                selectCar.add(stringVwgolf);

                imageButtonConnect.setVisibility(View.INVISIBLE);
                imageButtonCourierS.setVisibility(View.INVISIBLE);
                imageButtonTransit.setVisibility(View.INVISIBLE);
                imageButtonCourierB.setVisibility(View.INVISIBLE);


                connect.setVisibility(View.INVISIBLE);
                courierS.setVisibility(View.INVISIBLE);
                transit.setVisibility(View.INVISIBLE);
                courierB.setVisibility(View.INVISIBLE);


                final Toast toastGolf = Toast.makeText(this, "Wybrałeś" + " " + stringVwgolf , Toast.LENGTH_SHORT);
                toastGolf.show();

                new CountDownTimer(500, 1000)
                {
                    public void onTick(long millisUntilFinished) {
                        toastGolf.show();
                    }
                    public void onFinish() {
                        toastGolf.cancel();
                    }
                }.start();

                break;

            case R.id.imageButtonCourierB:

                imageButtonCourierB.setEnabled(true);
                imageButtonCourierB.setOnClickListener(null);
                mesage.setEnabled(true);
                comments.setEnabled(true);
                route.setEnabled(true);
                // switchDeffect.setEnabled(true);
                selectCar.add(stringCourierB);

                imageButtonConnect.setVisibility(View.INVISIBLE);
                imageButtonCourierS.setVisibility(View.INVISIBLE);
                imageButtonTransit.setVisibility(View.INVISIBLE);
                imageButtonGolf.setVisibility(View.INVISIBLE);


                connect.setVisibility(View.INVISIBLE);
                courierS.setVisibility(View.INVISIBLE);
                transit.setVisibility(View.INVISIBLE);
                vwgolf.setVisibility(View.INVISIBLE);



                final Toast toastCourierB = Toast.makeText(this, "Wybrałeś" + " " + stringCourierB , Toast.LENGTH_SHORT);
                toastCourierB.show();

                new CountDownTimer(500, 1000)
                {
                    public void onTick(long millisUntilFinished) {
                        toastCourierB.show();
                    }
                    public void onFinish() {
                        toastCourierB.cancel();
                    }
                }.start();

                break;


        }

        // finisch(); zamyka aplikacje kazdym przyciskiem
    }

    public void clickUserData(MenuItem item) {
        if (item.getItemId()==R.id.login){
            Intent i = new Intent(MainActivity.this, MenuPersonSave.class);
            startActivity(i);

        }
    }

    public void clickCompanyData(MenuItem item) {
        if (item.getItemId()==R.id.events){
            Intent j = new Intent(MainActivity.this, LabelPersonCompanyCard.class);
            startActivity(j);

        }
    }

    public void clickViewYourData (MenuItem item) {
        if (item.getItemId()==R.id.events){
            Intent k = new Intent(MainActivity.this, LabelPersonCompanyCard.class);
            startActivity(k);

        }
    }


}

