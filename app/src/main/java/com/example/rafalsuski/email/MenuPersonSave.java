package com.example.rafalsuski.email;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class MenuPersonSave extends AppCompatActivity implements
        TextWatcher, CompoundButton.OnCheckedChangeListener, EvidenceUser{

    public EditText edit_Login_firstname, edit_Login_lastname, edit_Email_job, edit_Email_add;
    public CheckBox rem_userpass;
    public Button schowSave;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    public static final String PREF_SHAREDPREF = "prefs";
    public static final String KEY_REMEMBER = "remember";
    public static final String KEY_FIRSTNAME = "firstname";
    public static final String KEY_LASTNAME = "lastname";
    public static final String KEY_EMAILJOB = "emailjob";
    public static final String KEY_EMAILADD = "emailadd";

    public TextView first;
    public TextView last;
    public TextView emailJob;
    public TextView emailAdd;
    public TextView labelName, labelEmail;



    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_person);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        sharedPreferences = getSharedPreferences(PREF_SHAREDPREF, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        edit_Login_firstname = (EditText) findViewById(R.id.edit_Login_firstname);
        edit_Login_lastname = (EditText) findViewById(R.id.edit_Login_lastname);
        edit_Email_job = (EditText) findViewById(R.id.edit_Email_job);
        edit_Email_add = (EditText) findViewById(R.id.edit_Email_add);
        rem_userpass = (CheckBox) findViewById(R.id.checkbox);

        first = (TextView) findViewById(R.id.empFirst);
        last = (TextView) findViewById(R.id.empLast);
        emailJob = (TextView) findViewById(R.id.empEmailjob);
        emailAdd = (TextView) findViewById(R.id.empEmailadd);
        schowSave = (Button) findViewById(R.id.schowSaveId);

        if (sharedPreferences.getBoolean(KEY_REMEMBER, false))
            rem_userpass.setChecked(true);
        else
            rem_userpass.setChecked(false);


        edit_Login_firstname.setText(sharedPreferences.getString(KEY_FIRSTNAME,""));
        edit_Login_lastname.setText(sharedPreferences.getString(KEY_LASTNAME, ""));
        edit_Email_job.setText(sharedPreferences.getString(KEY_EMAILJOB,""));
        edit_Email_add.setText(sharedPreferences.getString(KEY_EMAILADD,""));


        edit_Login_firstname.addTextChangedListener(this);
        edit_Login_lastname.addTextChangedListener(this);
        edit_Email_job.addTextChangedListener(this);
        edit_Email_add.addTextChangedListener(this);
        rem_userpass.setOnCheckedChangeListener(this);

        // Code identyficed save orientation Text View

        if (savedInstanceState != null){
            String savedFirstName = savedInstanceState.getString(KEY_FIRSTNAME);
            first.setText(savedFirstName);

            String savedLastName = savedInstanceState.getString(KEY_LASTNAME);
            last.setText(savedLastName);

            String savedEmailJob = savedInstanceState.getString(KEY_EMAILJOB);
            emailJob.setText(savedEmailJob);

            String savedEmailAdd = savedInstanceState.getString(KEY_EMAILADD);
            emailAdd.setText(savedEmailAdd);
        }

        else {
           // Toast.makeText(this, "This is your data", Toast.LENGTH_SHORT).show();
        }

        // Code click button saved view Text View

        String name = edit_Login_firstname.getText().toString();
        first.setText(name);

        String name1 = edit_Login_lastname.getText().toString();
        last.setText(name1);

        String name2 = edit_Email_add.getText().toString();
        emailAdd.setText(name2);

        String name3 = edit_Email_job.getText().toString();
        emailJob.setText(name3);


        //schowSave.setOnClickListener(new View.OnClickListener() {
           // @Override
          //  public void onClick(View v) {

               // String name = edit_Login_firstname.getText().toString();
               // labelName.setText(name);
           // }
       // });

        //activity_label_person_company_card = (TextView) findViewById(R.id.activity_label_person_company_card);
        //name = first.getText().toString();
        //activity_label_person_company_card.setText(name);


        //TextView test2 = (TextView) findViewById(R.id.test2);
        //name2 = last.getText().toString();
        //test2.setText(name2);


    }



    // Code save orientation Login, Email Text View
    @Override
    public  void onSaveInstanceState (Bundle saveInstanceState){

        saveInstanceState.putString(KEY_FIRSTNAME, first.getText().toString());
        saveInstanceState.putString(KEY_LASTNAME, last.getText().toString());
        saveInstanceState.putString(KEY_EMAILJOB, emailJob.getText().toString());
        saveInstanceState.putString(KEY_EMAILADD, emailAdd.getText().toString());

        super.onSaveInstanceState(saveInstanceState);
    }

    public void saveView(View view) {

        first.setText(edit_Login_firstname.getText().toString().trim());
        last.setText(edit_Login_lastname.getText().toString().trim());
        emailJob.setText(edit_Email_job.getText().toString().trim());
        emailAdd.setText(edit_Email_add.getText().toString().trim());

        finish();

    }


    // Code memory Login, Email Edit Text
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) { managePrefs();

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    private void managePrefs(){
        if (rem_userpass.isChecked()){
            editor.putString(KEY_FIRSTNAME, edit_Login_firstname.getText().toString().trim());
            editor.putString(KEY_LASTNAME, edit_Login_lastname.getText().toString().trim());
            editor.putString(KEY_EMAILJOB, edit_Email_job.getText().toString().trim());
            editor.putString(KEY_EMAILADD, edit_Email_add.getText().toString().trim());
            editor.putBoolean(KEY_REMEMBER, true);
            editor.apply();
        }
        else {
            editor.putBoolean(KEY_REMEMBER, false);
            editor.remove(KEY_LASTNAME);
            editor.remove(KEY_FIRSTNAME);
            editor.remove(KEY_EMAILJOB);
            editor.remove(KEY_EMAILADD);
            editor.apply();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) { managePrefs();

    }

}
