package com.example.rafalsuski.email;

import android.annotation.TargetApi;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.support.annotation.RequiresApi;

import java.util.Calendar;

@TargetApi(Build.VERSION_CODES.N)
@RequiresApi(api = Build.VERSION_CODES.N)


public interface DataClock {

    Calendar calendar = Calendar.getInstance();
    String currentDate = java.text.DateFormat.getDateInstance(DateFormat.DATE_FIELD).format(calendar.getTime());

    Calendar clock = Calendar.getInstance();
    SimpleDateFormat format = new SimpleDateFormat("HH:mm");
    String timeClock = format.format(clock.getTime());

}
